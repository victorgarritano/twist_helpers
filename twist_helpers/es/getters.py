from elasticsearch.helpers import scan


def get_news_from_spider(spider,
                         es_conn,
                         size,
                         start_date="2018-08-04T00:00:00",
                         end_date="2018-09-30T00:00:00",
                         verbose=False):
    """
    Get news (along with urls) from specific spider,
        within a time range that can be specified

    Args:
        spider (str): spider name
        es_conn (elasticsearch.Elasticsearch): ES low-level client
        size (int): Amount of returned hits
        start_date (datetime): Start date.
                               It must be on the following format:
                               YYYY-MM-DDTHH:MM:SS
        end_date (datetime): End date.
                             It must be on the following format:
                             YYYY-MM-DDTHH:MM:SS

    Return:
        hits (list): A list containing body of each hit
        urls (list): A list containingcorresponding url for each
                     element in hits
    """

    query = {
        "query": {
            "filtered": {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "date_published": {
                                        "gte": start_date,
                                        "lte": end_date,
                                    }
                                }
                            },
                            {
                                "term": {
                                    "spider": spider
                                }
                            }
                        ]
                    }
                }
            }
        },
        "size": size
    }

    hits, urls = [], []
    resp = scan(es_conn, index="crawler", doc_type="news", query=query)

    for hit in resp:
        if hit['_source']['body'] is not None:
            hits.append(hit['_source']['body'])
            urls.append(hit['_source']['url'])

    if verbose:
        print('hits: {0}'.format(len(hits)))

    return hits, urls



def get_score_by_id(_id):
    """
    TODO
    """
    pass
