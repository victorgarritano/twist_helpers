import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="twist_helpers",
    version="0.0.3",
    author="Victor Garritano",
    author_email="victor.garritano@twistsystems.com",
    description="A package collection to isolate data acess at TWIST",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/victorgarritano/twist_helpers",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
